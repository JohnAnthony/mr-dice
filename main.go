package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/kelseyhightower/envconfig"
)

func usage() {
	fmt.Printf("Usage is %s <host> <token>\n", os.Args[0])
}

func startBatch(host, token string) (string, error) {
	url := fmt.Sprintf(
		"https://%s/_matrix/client/r0/sync?access_token=%s",
		host,
		token,
	)
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}

	var sr struct {
		Next_Batch string
	}
	err = json.NewDecoder(resp.Body).Decode(&sr)
	if err != nil {
		return "", err
	}

	return sr.Next_Batch, nil
}

func sendMessage(host, token, room, msg string) error {
	url := fmt.Sprintf(
		"https://%s/_matrix/client/r0/rooms/%s/send/m.room.message?access_token=%s",
		host,
		"%21"+room[1:], // Replace "!"
		token,
	)
	body := struct {
		Msgtype string `json:"msgtype"`
		Body    string `json:"body"`
	}{
		Msgtype: "m.text",
		Body:    msg,
	}

	val, err := json.Marshal(body)
	if err != nil {
		return err
	}
	b := bytes.NewReader(val)

	_, err = http.Post(url, "application/json", b)
	return err
}

func main() {
	var conf struct {
		Host  string
		Token string
	}
	err := envconfig.Process("MRDICE", &conf)
	if err != nil {
		panic(err)
	}

	fmt.Println("Catching up...")
	since, err := startBatch(conf.Host, conf.Token)
	if err != nil {
		panic(err)
	}

	fmt.Println("Waiting on events...")
	url := fmt.Sprintf(
		"https://%s/_matrix/client/r0/sync?access_token=%s&timeout=%d&since=",
		conf.Host,
		conf.Token,
		60000,
	)
	for {
		resp, err := http.Get(url + since)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			time.Sleep(5 * time.Second)
			continue
		}

		var sr struct {
			Next_Batch string
			Rooms      struct {
				Join map[string]struct {
					Timeline struct {
						Events []struct {
							Content struct {
								Body    string
								Msgtype string
							}
						}
					}
				}
			}
		}
		err = json.NewDecoder(resp.Body).Decode(&sr)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			time.Sleep(5 * time.Second)
			continue
		}

		for room, data := range sr.Rooms.Join {
			for _, ev := range data.Timeline.Events {
				c := ev.Content
				if c.Msgtype != "m.text" {
					continue
				}
				if len(c.Body) < 6 {
					continue
				}
				if c.Body[:6] != "!roll " {
					continue
				}

				diceCode := c.Body[6:]
				fmt.Printf("%s: %s\n", room, diceCode)
				sendMessage(conf.Host, conf.Token, room, "foo")
			}
		}

		since = sr.Next_Batch
	}
}
